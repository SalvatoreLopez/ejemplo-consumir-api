const listaFrutas = document.querySelector("#listaFrutas");
const inputBuscar = document.querySelector("#inputBuscar");
const bottonBuscar = document.querySelector("#bottonBuscar");
const frutas = [
    {
        "Nombre": "Platano",
        "Valor" : 45.00
    },
    {
        "Nombre": "Uva",
        "Valor" : 5.00
    },
    {
        "Nombre": "Mango",
        "Valor" : 12.00
    },
    {
        "Nombre": "Papaya",
        "Valor" : 78.00
    },
    {
        "Nombre": "Fresa",
        "Valor" : 90.00
    }
];

const filtrar  = () => {
    listaFrutas.innerHTML = '';
    var palabraABuscar = inputBuscar.value.toLowerCase();
    frutas.forEach((fruta)=>{
        let  nombre = fruta.Nombre.toLowerCase();
        if( nombre.indexOf(palabraABuscar) !== -1 )
            listaFrutas.innerHTML += `<li>Nombre: ${ fruta.Nombre } - Valor: ${ fruta.Valor }</li>`
    });
    if( listaFrutas.innerHTML === '')
        listaFrutas.innerHTML = "No se econtro fruta";
}
inputBuscar.addEventListener('keyup',filtrar);
bottonBuscar.addEventListener('click',filtrar);
filtrar();