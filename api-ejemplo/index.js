var contenedor = document.getElementById('contenedor');

fetch("https://randomuser.me/api/?results=100",{
    method : 'get'  
})
.then((respuesta) =>  respuesta.json())
.then((informacion) => {
    informacion.results.forEach((usuarios) => {
        console.log(usuarios);
        contenedor.innerHTML += `<div class="col-6">
                                    <div class="card p-3 mt-2">
                                        <div class="media">
                                            <img class="border border-success mr-3 mt-4" src="${ usuarios.picture.large }" class="mr-3" alt="...">
                                            <div class="media-body">
                                                <p class="mb-0"><label class="font-weight-bold">Nombre:</label> ${ usuarios.name.title } ${usuarios.name.first} ${usuarios.name.last}</p>
                                                <p class="mb-0"><label class="font-weight-bold">Email:</label> ${ usuarios.email }</p>
                                                <p class="mb-0"><label class="font-weight-bold">Genero:</label> ${ usuarios.gender }</p>
                                                <p class="mb-0"><label class="font-weight-bold">Cell:</label> ${ usuarios.cell }</p>
                                                <p class="mb-0"><label class="font-weight-bold">Phone:</label> ${ usuarios.phone }</p>
                                                <p class="mb-0"><label class="font-weight-bold">Direccion:</label> ${usuarios.location.city}</p> 
                                            </div>
                                        </div>
                                    </div>
                                </div>`;
    });
})
.catch((error)=>console.log(error));